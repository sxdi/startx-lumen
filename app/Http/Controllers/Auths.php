<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 use Illuminate\Support\Facades\Auth;

class Auths extends Controller
{
    //
    public function index(Request $request)
    {
        $credentials = $request->only(['email', 'password']);
        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function profile(Request $request)
    {
        return response()->json(['user' => Auth::user()->id]);
    }
}
